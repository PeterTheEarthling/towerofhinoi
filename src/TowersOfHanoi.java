/**
 * Steven Szczeszynski
 * Peter Szczeszynski
 *
 * 3/28/2019
 *
 * Represents the classic Towers of Hanoi puzzle.
 */

public class TowersOfHanoi
{

    public int totalDisks;

    //-----------------------------------------------------------------
    //  Sets up the puzzle with the specified number of disks.
    //-----------------------------------------------------------------
    public TowersOfHanoi(int disks)
    {
        totalDisks = disks;

    }

    //-----------------------------------------------------------------
    //  Performs the initial call to moveTower to solve the puzzle.
    //  Moves the disks from tower 1 to tower 3 using tower 2.
    //-----------------------------------------------------------------
    public void solve()
    {
        int start = 1;
        int end = 3;
        //populate the list of blocks
        for(int i = 0; i < totalDisks; i ++){
            MyCanvas.allBlocks.add
                    (new Block(start,i,1.2- ((double) i/totalDisks)));
        }
        moveTower(totalDisks, start, end, 2);
    }

    //-----------------------------------------------------------------
    //  Moves the specified number of disks from one tower to another
    //  by moving a subtower of n-1 disks out of the way, moving one
    //  disk, then moving the subtower back. Base case of 1 disk.
    //-----------------------------------------------------------------
    private void moveTower(int numDisks, int start, int end, int temp)
    {
        if (numDisks == 1)
            moveOneDisk(start, end);
        else
        {
            moveTower(numDisks-1, start, temp, end);
            moveOneDisk(start, end);
            moveTower(numDisks-1, temp, end, start);
        }
    }

    //-----------------------------------------------------------------
    //  Prints instructions to move one disk from the specified start
    //  tower to the specified end tower.
    //-----------------------------------------------------------------
    private void moveOneDisk(int start, int end)
    {
        System.out.println("Move one disk from " + start + " to " +
                end);

        /*
        1. Find the disk in MyCanvas to move (last one that has location of start)
        2. Move it by changing location and height
         */
        Block foundBlock = null;

        for(int i = MyCanvas.allBlocks.size()-1; i >= 0; i --){
            if(MyCanvas.allBlocks.get(i).location == start) {
                foundBlock = MyCanvas.allBlocks.get(i);
                break;
            }

        }

        int countAtPeg = 0;
        for(Block b : MyCanvas.allBlocks){
            countAtPeg += b.location == end ? 1 : 0;
        }
        if(foundBlock != null){
            foundBlock.location = end;
            foundBlock.height = countAtPeg;
        }



        try {
            SolveTowers.myCanvas.repaint();
            Thread.sleep(250);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}