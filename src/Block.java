/**
 * Steven Szczeszynski
 * Peter Szczeszynski
 *
 * 3/28/2019
 *
 * Represents a block/disk on the puzzle
 */
public class Block {
    //the location of the block from left to right
    public int location;
    //the height of the block (0 is the bottom)
    public int height;
    public double size;

    /**
     * Creates a new Block
     * @param location 0 is the left peg
     * @param height 0 is the bottom
     */
    public Block(int location, int height, double size){
        this.location = location;
        this.height = height;
        this.size = size;
    }
}
