import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * Steven Szczeszynski
 * Peter Szczeszynski
 *
 * 3/28/2019
 *
 * Holds all the rectangles
 */

public class MyCanvas extends JComponent {

    public static ArrayList<Block> allBlocks
            = new ArrayList<>();

    public void paint(Graphics g) {
        //we are going to set the color to red
        g.setColor(new Color(0,0,0));


        //defines where pegs are located
        int pegSpacing = 350;
        int horizontalPegOffset = 200;
        //greater for more down
        int verticalPegOffset = 50;

        //offset to tune block locations - greater for more to the left
        int horizontalBlockOffset = 85;
        //offset to tune block locations - greater for more down
        int verticalBlockOffset = 430;

        //define peg locations in terms of constant offset and spacing variables
        int location1 = horizontalPegOffset;
        int location2 = horizontalPegOffset + pegSpacing;
        int location3 = horizontalPegOffset + (2*pegSpacing);


        //pegs for disks
        g.fillRect(location1,verticalPegOffset,30,450);
        g.fillRect(location2,verticalPegOffset,30,450);
        g.fillRect(location3,verticalPegOffset,30,450);


        //color of the blocks
        g.setColor(new Color(255,0,0));
        //variable that changes the color of the disks
        int i = 0;

        for(Block b : allBlocks){

            //determine color for the next disk

            switch(i) {
                case (0):
                    g.setColor(new Color(255,0,0));
                    break;
                case (1):
                    g.setColor(new Color(255,(255/2),0));
                    break;
                case (2):
                    g.setColor(new Color(255,255,0));
                    break;
                case (3):
                    g.setColor(new Color(0,255,0));
                    break;
                case (4):
                    g.setColor(new Color(0,0,255));
                    break;
                default:
                    g.setColor(new Color(100,10,100));
            }

            i++;

            //set block locations based on constants and peg number
            int x = location1 + (int)(100 - (200*(b.size/2)));
            if(b.location == 2){
                x = location2 + (int)(100 - (200*(b.size/2)));
            }
            if(b.location == 3){
                x = location3 + (int)(100 - (200*(b.size/2)));
            }
            g.fillRect(x-horizontalBlockOffset,verticalBlockOffset - (b.height*75), (int) (b.size * 200.0),50);
        }

    }
}
