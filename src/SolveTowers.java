import javax.swing.*;
import java.awt.*;
import java.util.Scanner;

/**
 * Steven Szczeszynski
 * Peter Szczeszynski
 *
 * 3/28/2019
 *
 * Main driver class that sets up puzzle
 */
public class SolveTowers {

    public static MyCanvas myCanvas;
    //-----------------------------------------------------------------
    //  Creates a TowersOfHanoi puzzle and solves it.
    //-----------------------------------------------------------------
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        System.out.println("How many disks?");
        int numberOfDisks = scan.nextInt();

        MyJFrame myMyJFrame = new MyJFrame();
        //set the size of the jFrame
        myMyJFrame.setSize(new Dimension(1200,700));

        //Do layout before we set visible
//        JPanel jPanel = new JPanel();
//        jPanel.setSize(new Dimension(500, 500));
//        myMyJFrame.add(jPanel);



        myCanvas = new MyCanvas();
        myMyJFrame.getContentPane().add(myCanvas);


        myMyJFrame.setVisible(true);



        for(;;){
            MyCanvas.allBlocks.clear();
            TowersOfHanoi towers = new TowersOfHanoi(numberOfDisks);
            towers.solve();
        }

    }
}
